package br.vilugao.android.diycarcontroller;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.UUID;

final class BTSPPCarrinhoController extends Thread implements CarrinhoController {
	private static final UUID BTSPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
	private static final String TAG = "BTSPPCarrinhoController";

	private final Handler callbackHandler;
	public static final int MSG_FALHA_CONEXAO = 1;
	public static final int MSG_CONECTADO = 2;
	public static final int MSG_FALHA_ENVIO = 3;
	public static final int MSG_CONEXAO_FINALIZADA = 4;

	private final BluetoothSocket socket;
	private final Queue<String> filaComandos = new ArrayDeque<>();
	private boolean rodando = true, conectado;
	private int velocidade, direcao;

	public BTSPPCarrinhoController(final BluetoothDevice d, final Handler h)
			throws IOException {
		callbackHandler = h;
		Log.i(TAG, "Criando conexão SPP ao dispositivo BT " + d.getAddress() + '.');
		socket = d.createRfcommSocketToServiceRecord(BTSPP_UUID);
		start();
	}

	BTSPPCarrinhoController() {
		callbackHandler = null;
		socket = null;
		conectado = true;
	}

	@Override
	public void run() {
		BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
		try {
			socket.connect();
			Log.d(TAG, "Conectado!");
			callbackHandler.obtainMessage(MSG_CONECTADO).sendToTarget();
			try (final OutputStream os = socket.getOutputStream()) {
				os.write("ujlkpr".getBytes());
				synchronized (this) {
					conectado = true;
				}
				while (rodando) {
					String cmd = filaComandos.poll();
					while (cmd != null) {
						Log.v(TAG, "Enviando o comando '" + cmd + "'.");
						try {
							os.write(cmd.getBytes());
						} catch (final IOException e) {
							Log.e(TAG, "Erro ao enviar o comando.", e);
							callbackHandler.obtainMessage(MSG_FALHA_ENVIO).sendToTarget();
						}
						cmd = filaComandos.poll();
					}
					synchronized (this) {
						wait();
					}
				}
			}
			callbackHandler.obtainMessage(MSG_CONEXAO_FINALIZADA).sendToTarget();
		} catch (final IOException connectEx) {
			Log.e(TAG, "Erro de conexão.", connectEx);
			callbackHandler.obtainMessage(MSG_FALHA_CONEXAO).sendToTarget();
		} catch (final InterruptedException ignored) {
		} finally {
			try {
				socket.close();
				Log.i(TAG, "Conexão finalizada.");
			} catch (final IOException closeEx) {
				Log.e(TAG, "Não foi possível fechar a conexão.", closeEx);
			}
			synchronized (this) {
				conectado = true;
			}
		}
	}

	private void enviaComando(final String cmd) {
		if (!isAlive()) {
			Log.w(TAG, "Sem conexão. Comando '" + cmd + "' descartado.");
			return;
		}
		synchronized (this) {
			filaComandos.add(cmd);
			notify();
		}
	}

	@Override
	public void setFreio(final boolean v) {
		enviaComando(v ? "P" : "p");
	}

	@Override
	public void setAcelerador(final int v) {
		assert v >= 0 && v <= 15;
		if (v != 0 || v != velocidade) {
			velocidade = v;
			enviaAceleracao();
		}
	}

	@Override
	public void setDirecao(final int d) {
		assert d >= -7 && d <= 7;
		if (velocidade != 0 || d != direcao) {
			direcao = d;
			if (conectado) {
				enviaAceleracao();
			}
		}
	}

	private void enviaAceleracao() {
		enviaComando(String.format("%x%x", velocidade, direcao & 0xf));
	}

	@Override
	public void engateParaRe(final boolean re) {
		enviaComando(re ? "R" : "r");
	}

	@Override
	public void setFarol(final boolean v) {
		enviaComando(v ? "U" : "u");
	}

	@Override
	public void setSetas(final PosicaoSetas ps) {
		enviaComando(ps == PosicaoSetas.Esquerda ? "J"
				: ps == PosicaoSetas.Direita ? "L"
				: "jl");
	}

	@Override
	public void setPiscaAlerta(final boolean v) {
		enviaComando(v ? "K" : "k");
	}

	public synchronized void desconecta() {
		rodando = false;
		notify();
	}
}
