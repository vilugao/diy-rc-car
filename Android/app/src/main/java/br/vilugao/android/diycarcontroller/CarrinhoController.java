package br.vilugao.android.diycarcontroller;

interface CarrinhoController {
	enum PosicaoSetas {Desligadas, Esquerda, Direita}

	void setFreio(boolean v);
	void setAcelerador(int veloc);
	void setDirecao(int dir);
	void engateParaRe(boolean re);
	void setFarol(boolean v);
	void setSetas(PosicaoSetas ps);
	void setPiscaAlerta(boolean v);
}
