package br.vilugao.android.diycarcontroller;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;

public final class ControleActivity extends Activity
		implements SensorEventListener, View.OnTouchListener {
	private static final String TAG = "ControleActivity";

	private SensorManager sensorManager;
	private BTSPPCarrinhoController ctrl;
	private final boolean aceleracaoAnalogica = false;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_controle);

		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		final BluetoothDevice dev
				= getIntent().getParcelableExtra(MainActivity.DISPOSITIVO_BT);
		if (dev == null) {
			ctrl = new BTSPPCarrinhoController();
			return;
		}
		try {
			ctrl = new BTSPPCarrinhoController(dev, getHandler());
		} catch (final IOException e) {
			Toast.makeText(this, R.string.btconn_connfailed_toast,
					Toast.LENGTH_LONG).show();
			Log.e(TAG, "Não foi possível conectar.", e);
			return;
		}

		findViewById(R.id.pedal_freia).setOnTouchListener(this);
		findViewById(R.id.pedal_acelera).setOnTouchListener(this);
		((CompoundButton) findViewById(R.id.cambio)).setOnCheckedChangeListener(
				(btn, isChecked) -> ctrl.engateParaRe(isChecked));

		((CompoundButton) findViewById(R.id.farol)).setOnCheckedChangeListener(
				(btn, isChecked) -> ctrl.setFarol(isChecked));

		new SetasListener();

		((CompoundButton) findViewById(R.id.piscaalerta)).setOnCheckedChangeListener(
				(btn, isChecked) -> ctrl.setPiscaAlerta(isChecked));
	}

	private Handler getHandler() {
		return new Handler(Looper.getMainLooper()) {
			@Override
			public void handleMessage(final Message msg) {
				final View dv = getWindow().getDecorView();
				switch (msg.what) {
				case BTSPPCarrinhoController.MSG_FALHA_CONEXAO:
					Toast.makeText(ControleActivity.this,
							R.string.btconn_connfailed_toast,
							Toast.LENGTH_SHORT).show();
					break;
				case BTSPPCarrinhoController.MSG_CONECTADO:
					findViewById(R.id.signal_low).setVisibility(View.GONE);
					findViewById(R.id.connect_status).setVisibility(View.VISIBLE);
					dv.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
					break;
				case BTSPPCarrinhoController.MSG_CONEXAO_FINALIZADA:
					findViewById(R.id.connect_status).setVisibility(View.GONE);
					findViewById(R.id.signal_low).setVisibility(View.VISIBLE);
					dv.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
					Toast.makeText(ControleActivity.this,
							R.string.btconn_closed_toast,
							Toast.LENGTH_SHORT).show();
					break;
				case BTSPPCarrinhoController.MSG_FALHA_ENVIO:
					Toast.makeText(ControleActivity.this,
							R.string.btconn_txfailed_toast,
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		};
	}

	@Override
	protected void onResume() {
		super.onResume();
		Sensor s = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		if (s == null) {
			s = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		}
		sensorManager.registerListener(this, s, 200_000);
	}

	@Override
	public void onAccuracyChanged(final Sensor s, final int accuracy) {}

	@Override
	public void onSensorChanged(final SensorEvent evt) {
		final int tipo = evt.sensor.getType();
		if (tipo == Sensor.TYPE_GRAVITY || tipo == Sensor.TYPE_ACCELEROMETER) {
			final int dir = Math.round(7 * 2 / (float) Math.PI
					* (float) Math.atan(evt.values[1] / evt.values[0]));
			((ProgressBar) findViewById(R.id.barra_direcao)).setProgress(dir + 7);
			ctrl.setDirecao(dir);
		}
	}

	@Override
	public boolean onTouch(final View v, final MotionEvent evt) {
		final int id = v.getId();
		if (id == R.id.pedal_freia) {
			switch (evt.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				ctrl.setFreio(true);
				break;
			case MotionEvent.ACTION_UP:
				ctrl.setFreio(false);
				break;
			}
		} else if (id == R.id.pedal_acelera) {
			switch (evt.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				if (!aceleracaoAnalogica) {
					ctrl.setAcelerador(15);
					break;
				}
			case MotionEvent.ACTION_MOVE:
				if (aceleracaoAnalogica)
					ctrl.setAcelerador(Math.min(15,
							Math.round(15 * evt.getPressure())));
				break;
			case MotionEvent.ACTION_UP:
				ctrl.setAcelerador(0);
				break;
			}
		}
		v.performClick();
		return false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
	}

	@Override
	protected void onDestroy() {
		if (ctrl != null) {
			ctrl.desconecta();
			ctrl = null;
		}
		super.onDestroy();
	}

	final class SetasListener implements CompoundButton.OnCheckedChangeListener {
		private final CompoundButton esqBtn = (CompoundButton) findViewById(R.id.seta_esq);
		private final CompoundButton dirBtn = (CompoundButton) findViewById(R.id.seta_dir);

		public SetasListener() {
			esqBtn.setOnCheckedChangeListener(this);
			dirBtn.setOnCheckedChangeListener(this);
		}

		@Override
		public void onCheckedChanged(final CompoundButton btn, final boolean isChecked) {
			if (!isChecked) {
				ctrl.setSetas(CarrinhoController.PosicaoSetas.Desligadas);
			} else if (esqBtn.equals(btn)) {
				dirBtn.setChecked(false);
				ctrl.setSetas(CarrinhoController.PosicaoSetas.Esquerda);
			} else if (dirBtn.equals(btn)) {
				esqBtn.setChecked(false);
				ctrl.setSetas(CarrinhoController.PosicaoSetas.Direita);
			}
		}
	}
}
