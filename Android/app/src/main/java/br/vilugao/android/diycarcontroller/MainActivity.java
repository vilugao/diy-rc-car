package br.vilugao.android.diycarcontroller;

import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class MainActivity extends ListActivity {
	private static final String TAG = "MainActivity";
	private static final String NOME = "nome";
	private static final String ENDERECO = "endereço";
	private static final int REQUEST_ENABLE_BT = 100;

	public static final String DISPOSITIVO_BT = "br.vilugao.android.diycarrc.DISPOSITIVO_BT";

	private List<BluetoothDevice> dispositivos;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.mainactivity_title);
		listaDispositivos();
	}

	private void listaDispositivos() {
		final BluetoothAdapter bta = BluetoothAdapter.getDefaultAdapter();
		if (bta == null) {
			Toast.makeText(this, R.string.bt_noadapter_toast,
					Toast.LENGTH_LONG).show();
			startActivity(new Intent(this, ControleActivity.class));
			finish();
			return;
		}
		if (!bta.isEnabled()) {
			final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			return;
		}

		dispositivos = new ArrayList<>();
		{
			final Set<BluetoothDevice> devices = bta.getBondedDevices();
			for (final BluetoothDevice dev : devices) {
				final int devClass = dev.getBluetoothClass().getMajorDeviceClass();
				if (devClass == BluetoothClass.Device.Major.TOY
						|| devClass == BluetoothClass.Device.Major.MISC
						|| devClass == BluetoothClass.Device.Major.UNCATEGORIZED
				) {
					dispositivos.add(dev);
				} else {
					Log.d(TAG, String.format("Dispositivo fora: %s (%s) - 0x%x.",
							dev.getAddress(), dev.getName(), devClass));
				}
			}
		}
		if (dispositivos.isEmpty()) {
			Toast.makeText(this, R.string.bt_nopairedcars_toast,
					Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		final List<Map<String, String>> lista = new ArrayList<>(dispositivos.size());
		for (final BluetoothDevice d : dispositivos) {
			final Map<String, String> map = new HashMap<>();
			map.put(NOME, d.getName());
			map.put(ENDERECO, d.getAddress());
			lista.add(map);
		}
		setListAdapter(new SimpleAdapter(this, lista,
				android.R.layout.simple_list_item_2,
				new String[]{NOME, ENDERECO},
				new int[]{android.R.id.text1, android.R.id.text2}));
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode,
									final Intent data) {
		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == RESULT_OK) {
				listaDispositivos();
			} else {
				Toast.makeText(this, R.string.bt_noadapter_toast,
						Toast.LENGTH_LONG).show();
				finish();
			}
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onListItemClick(final ListView l, final View v,
								   final int pos, final long id) {
		final BluetoothDevice dev = dispositivos.get(pos);
		Log.d(TAG, "Dispositivo selecionado: " + dev.getName() + '.');
		final Intent intent = new Intent(this, ControleActivity.class);
		intent.putExtra(DISPOSITIVO_BT, dev);
		startActivity(intent);
	}
}
