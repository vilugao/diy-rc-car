#include <avr/interrupt.h>
#include <avr/sleep.h>

#define PBFREIO PB4
#define PBREV PB5

enum {
  // CONTROLE
  FREIA = 'P',

  // CAMBIO
  REV = 'R',

  // LUZES
  FAROL = 'U',
  SETA_DIREITA = 'L',
  SETA_ESQUERDA = 'J',
  PISCA_ALERTA = 'K',
};

#define setas GPIOR0
enum {
  SDIREITA,
  SESQUERDA,
  SPALERTA,
};

static void ioinit(void)
{
  // Desabilita funções sem uso.
  PRR = _BV(PRTWI) | _BV(PRTIM2) | _BV(PRSPI) | _BV(PRADC);
  DIDR0 = _BV(ADC5D) | _BV(ADC4D) | _BV(ADC3D) | _BV(ADC2D) | _BV(ADC1D) | _BV(ADC0D);
  ACSR = _BV(ACD);
  DIDR1 = _BV(AIN1D) | _BV(AIN0D);

  // PWM para ponte H (phase correct mode).
  TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM00);
  TCCR0B = _BV(CS00);

  // Temporizador para setas.
  TCCR1A = _BV(COM1A0) | _BV(COM1B0);
  TCCR1B = _BV(WGM12) | _BV(CS12);
  OCR1A = F_CPU / (2 * 256) - 1;

  // Saídas para luzes.
  DDRB = _BV(DDB1) | _BV(DDB2) | _BV(DDB3) | _BV(DDB4) | _BV(DDB5);
  setas = _BV(SPALERTA);
  // Saídas para ponte H.
  DDRD = _BV(DDD2) | _BV(DDD3) | _BV(DDD4) | _BV(DDD5) | _BV(DDD6) | _BV(DDD7);
  PORTD = _BV(PD2) | _BV(PD4);

  // UART RX a 9600 bauds.
#define BAUD 9600
#include <util/setbaud.h>
  UBRR0 = UBRR_VALUE;
#if USE_2X
  UCSR0A = _BV(U2X);
#else
  UCSR0A = 0;
#endif
  UCSR0B = _BV(RXEN0) | _BV(RXCIE0);
  UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
}

static void processa(void);

int main(void)
{
  ioinit();

  SMCR = _BV(SE);
  sei();
  sleep_cpu();

  do {
    processa();
    sleep_cpu();
  } while (1);
}

#define ASCIILOWBIT ('A' ^ 'a')

static uint8_t hextoi(const uint8_t c)
{
  return c >= 'A' ? (c & ~ASCIILOWBIT) - ('A' - 10) : c - '0';
}

static void processa(void)
{
  uint8_t c = UDR0;
  switch (c) {
    case FREIA:
      PORTB |= _BV(PBFREIO);
      PORTD = 0;
      OCR0B = OCR0A = 255;
      return;
    case FREIA | ASCIILOWBIT:
      PORTB &= ~_BV(PBFREIO);
      PORTD = bit_is_set(PINB, PBREV) ? _BV(PD3) | _BV(PD7) : _BV(PD2) | _BV(PD4);
      OCR0B = OCR0A = 0;
      return;

    case REV:
      PORTB |= _BV(PBREV);
      if (bit_is_clear(PINB, PBFREIO))
        PORTD = _BV(PD3) | _BV(PD7);
      return;
    case REV | ASCIILOWBIT:
      PORTB &= ~_BV(PBREV);
      if (bit_is_clear(PINB, PBFREIO))
        PORTD = _BV(PD2) | _BV(PD4);
      return;

    case FAROL:
      PORTB |= _BV(PB3);
      return;
    case FAROL | ASCIILOWBIT:
      PORTB &= ~_BV(PB3);
      return;

    case SETA_ESQUERDA:
      setas |= _BV(SESQUERDA);
    case SETA_DIREITA | ASCIILOWBIT:
      setas &= ~_BV(SDIREITA);
      break;
    case SETA_DIREITA:
      setas |= _BV(SDIREITA);
    case SETA_ESQUERDA | ASCIILOWBIT:
      setas &= ~_BV(SESQUERDA);
      break;

    case PISCA_ALERTA:
      setas |= _BV(SPALERTA);
      break;
    case PISCA_ALERTA | ASCIILOWBIT:
      setas &= ~_BV(SPALERTA);
      break;

    default:
      c = hextoi(c);
      if (c > 0xF)
        return;

      loop_until_bit_is_set(UCSR0A, RXC0);
      int8_t d = hextoi(UDR0);
      if (bit_is_set(PINB, PBFREIO) || d & ~0xF)
        return;

      c |= (uint8_t)(c << 4);
      d = (d ^ 8) - 8; // Extensão de sinal.

      if (!c) {
        OCR0B = OCR0A = 0;
      } else if (!d) { // Para reto.
        OCR0B = OCR0A = c;
      } else if (d < 0) { // Para esquerda.
        OCR0A = c * (8 + d) / 8;
        OCR0B = c;
      } else { // Para direita.
        OCR0B = c * (8 - d) / 8;
        OCR0A = c;
      }
      return;
  }

  if (bit_is_set(setas, SPALERTA)) {
    DDRB |= _BV(DDB1);
    DDRB |= _BV(DDB2);
  } else {
    DDRB &= ~_BV(DDB1);
    DDRB &= ~_BV(DDB2);
    if (bit_is_set(setas, SDIREITA))
      DDRB |= _BV(DDB2);
    else if (bit_is_set(setas, SESQUERDA))
      DDRB |= _BV(DDB1);
  }
}

EMPTY_INTERRUPT(USART_RX_vect);
